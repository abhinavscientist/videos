import React from 'react';
import SearchBar from './SearchBar';
import YoutubeAPI from '../api/YoutubeAPI';
import VideoList from './VideoList';
import VideoDetails from './VideoDetails';

class App extends React.Component {
    state = {videolist: []};
    onSearchSubmit = async (query) => {
        const params = {
            'maxResults': '5',
            'part': 'snippet',
            'q': query,
            'key': "AIzaSyBuvGDTE-JRk7v_clcyTcL0He5hdnBzwSk",
            'type': '',
        };
        YoutubeAPI.get('/youtube/v3/search', {params})
        .then((response) => {
            this.setState({videolist: response.data.items});
            // console.log(response);
            window.list = this.state.videolist;
            console.log(this.state.videolist);
        })
        .catch((response) => {
            console.log('Error: Youtube API did not return results');
            console.log(response);
        });
    }
    render() {
        return (
            <div className="container">
                <SearchBar onSearchSubmit={this.onSearchSubmit}></SearchBar>
                <div className="row">
                    <div className="col-8">
                        <VideoDetails videolist={this.state.videolist}></VideoDetails>
                    </div>
                    <div className="col-4">
                        <VideoList videolist={this.state.videolist}></VideoList>
                    </div>
                </div>
            </div>
        );
    }

};

export default App;