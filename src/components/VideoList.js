import React from 'react';
import VideoItem from './VideoItem';

const VideoList = (props) => {
    const videos = props.videolist.map((videoitem) => {
        return <VideoItem key={videoitem.etag} videoitem={videoitem}></VideoItem>
    });
    return <div className="container video-list">{videos}</div>
}

export default VideoList;