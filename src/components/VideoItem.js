import React from 'react';

class VideoItem extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="card" style={{ width: '18rem' }}>
                <a target="_blank" rel="noopener noreferrer" href={`https://www.youtube.com/watch?v=${this.props.videoitem.id.videoId}`}>
                <img  className="card-img-top" src={this.props.videoitem.snippet.thumbnails.medium.url} alt="alt" /></a>
                <div className="card-body">
                    <h5 className="card-title">{this.props.videoitem.snippet.title}</h5>
                    <p className="card-text">{this.props.videoitem.snippet.description}</p>
                    <a target="_blank" rel="noopener noreferrer" href={`https://www.youtube.com/watch?v=${this.props.videoitem.id.videoId}`} className="btn btn-primary">Play Video</a>
                </div>
            </div>
        );
    }
}

export default VideoItem;