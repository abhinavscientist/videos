import React from 'react';

class VideoDetails extends React.Component {
    render() {
        // console.log(this.props.videolist.length);
        let video;
        if (this.props.videolist.length > 0) {
            let videoitem = this.props.videolist[0];
            video = <div className="card" style={{ width: '42rem' }}>
                <a target="_blank" rel="noopener noreferrer" href={`https://www.youtube.com/watch?v=${videoitem.id.videoId}`}>
                    <img className="card-img-top" src={videoitem.snippet.thumbnails.high.url} alt="alt" /></a>
                <div className="card-body">
                    <h5 className="card-title">{videoitem.snippet.title}</h5>
                    <p className="card-text">{videoitem.snippet.description}</p>
                    <a target="_blank" rel="noopener noreferrer" href={`https://www.youtube.com/watch?v=${videoitem.id.videoId}`} className="btn btn-primary">Play Video</a>
                </div>
            </div>
        }
        return (
            <div className="video-list">{video}</div>
            
        );
    }
}

export default VideoDetails;
