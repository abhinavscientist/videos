import React from 'react';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {query: ''};
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.onSearchSubmit(this.state.query);
    };

    render() {
        return (
            <form onSubmit={this.onFormSubmit} className="form-inline">
                <div className="form-group">
                    <label htmlFor="searchquery">Search Youtube </label>
                    <input type="text" className="form-control" value={this.state.query} onChange={(e) => {this.setState({query: e.target.value})}}/>
                </div>
            </form>
        );
    }
}

export default SearchBar;

