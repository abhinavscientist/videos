// import axios from 'axios';

const axios = require("axios");

const YoutubeAPI = axios.create({
    baseURL: "https://www.googleapis.com/"
});

export default YoutubeAPI;
